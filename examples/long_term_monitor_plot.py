# -*- coding: utf-8 -*-
"""long_term_monitor_plot.py

This script repeatedly records readouts from the Thorlabs TSP01B sensor.
Requires a connected TSP01 Rev.B sensor.

Author: Dima Pustakhod
Copyright: TU/e
"""
# from time import time, sleep, gmtime, strftime
import numpy as np
import pandas as pd

# Settings
# --------
fname = '_output_data/20191009T092256 - data.csv'

df = pd.read_csv(fname, delimiter=',')

df['Time'] = pd.to_datetime(df['time'], unit='s')

df.drop(['time'], inplace=True, axis='columns')

df.set_index('Time', inplace=True)

ax = df[['th0', 'th1', 'th2', 'h0']].plot(secondary_y=['h0'])
ax.set_ylabel('Temperature')
ax.right_ax.set_ylabel('Humidity')
