py_thorlabs_tsp Changelog
=========================

Version 0.0.8 (unreleased)
--------------------------
- No changes yet.

Version 0.0.7 (2021-03-17)
--------------------------
- Fixed issue with connection to multiple devices (#1).
