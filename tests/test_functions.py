# -*- coding: utf-8 -*-
"""test_functions.py

This script tests low-level wrapped functions of the TSP01B DLL.
Requires a connected TSP01 Rev.B sensor.

Author: Dima Pustakhod
Copyright: TU/e

"""
from time import sleep

import os
from path import Path
import sys


p = Path(os.getcwd()).parent
if p not in sys.path:
    sys.path.append(p)


from py_thorlabs_tsp import _calibrationMessage, _close, _errorMessage
from py_thorlabs_tsp import _errorQuery
from py_thorlabs_tsp import _findRsrc, _getExtRefVoltage, _getExtSerResist
from py_thorlabs_tsp import _getHumidityData, _getHumSensOffset
from py_thorlabs_tsp import _getProductionDate
from py_thorlabs_tsp import _getRsrcInfo, _getRsrcName, _getTemperatureData
from py_thorlabs_tsp import _getTempSensOffset, _getThermistorExpParams
from py_thorlabs_tsp import _getThermRes
from py_thorlabs_tsp import _identificationQuery, _init, _isServiceMode
from py_thorlabs_tsp import _measHumidity, _measTemperature, _reset
from py_thorlabs_tsp import _revisionQuery
from py_thorlabs_tsp import _selfTest, _setDeviceName

# Not tested because the service mode is not available
from py_thorlabs_tsp import _setExtRefVoltage, _setExtSerResist
from py_thorlabs_tsp import _setHumSensOffset, _setProductionDate
from py_thorlabs_tsp import _setSerialNr, _setServiceMode, _setTempSensOffset


e, dev_count = _findRsrc()
print(hex(e), dev_count)

e, res_name = _getRsrcName(0)
print(hex(e), res_name)

e, model_name, serial_no, manufacturer_name, resource_in_use = _getRsrcInfo(0)
print(hex(e), model_name, serial_no, manufacturer_name, resource_in_use)

# reset=False to save the lifetime of the devices's EEPROM
e, instr_handle = _init(res_name, True, False)
print(hex(e), instr_handle)

e1, err = _errorMessage(instr_handle, e)
print(hex(e), instr_handle, hex(e1), err)

print(_errorQuery(instr_handle))

print(_getProductionDate(instr_handle))

print(_getExtRefVoltage(instr_handle))

print(_getExtSerResist(instr_handle))

for attr in (0, 1, 2, 3):
    print(_getHumidityData(instr_handle, attr))

for attr in (0, 1, 2):
    print(_getHumSensOffset(instr_handle, attr))

for ch in (1, 2, 3):
    for attr in (0, 1, 2, 3):
        sleep(0.1)
        print(_getTemperatureData(instr_handle, ch, attr))

for ch in (1, 2, 3):
    for attr in (0, 1, 2):
        sleep(0.1)
        print(_getTempSensOffset(instr_handle, ch, attr))

for ch in (2, 3):  # External channels only
    for attr in (0, 1, 2, 3):
        sleep(0.1)
        print(_getThermistorExpParams(instr_handle, ch, attr))

for ch in (2, 3):  # External channels only
    for attr in (0, 1, 2, 3):
        sleep(0.1)
        print(_getThermRes(instr_handle, ch, attr))

print(_identificationQuery(instr_handle))

print(_isServiceMode(instr_handle))

print(_measHumidity(instr_handle))

for ch in (1, 2, 3):
    print(_measTemperature(instr_handle, ch))

# Excluded to save the lifetime of the devices's EEPROM.
# print(_reset(instr_handle))

print(_revisionQuery(instr_handle))

e, test_res, descr = _selfTest(instr_handle)
e1, err = _errorMessage(instr_handle, e)
print(e, test_res, descr, e1, err)

e = _setDeviceName(instr_handle, 'new name')
e1, err = _errorMessage(instr_handle, e)
print(e, e1, err)

print(_close(instr_handle))
